﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CodeBox.ascx.cs" Inherits="assignment2a.WebUserControl1" %>

<div id="form1">
<div class="new_footer">
        <div class="col-md-2 col-xs-12"><label>Name:</label></div>
        <div class="col-md-5 col-xs-12">
            <asp:TextBox runat="server" ID="info_name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="info_name"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-xs-12"><label class="formlabel">Email:</label></div>
        <div class="col-md-5 col-xs-12">
            <asp:TextBox runat="server" ID="info_email"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="info_email"></asp:RequiredFieldValidator>
        </div>
    </div>
<div class="row">
        <asp:Button runat="server" OnClick="Submit_Form" Text="Submit"/>
    </div>
    <asp:Label runat="server" ID="thankyou_msg"></asp:Label>
</div>