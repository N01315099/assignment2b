﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace assignment2a
{
    public partial class WebUserControl1 : System.Web.UI.UserControl
    {

        public string UserName
        {
            get { return (string)ViewState["UserName"]; }
            set { ViewState["UserName"] = value; }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            

        }

        protected void Submit_Form(object sender, EventArgs e)
        {
            thankyou_msg.Text = "Thank you for submitting and your interest in " + UserName;
        }
    }
}